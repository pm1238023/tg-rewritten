# Setup

1. Get Node.js (If you haven't)
2. Get ESLint (If you haven't) (`npm i -g eslint` in a terminal)
3. Get the Glitch extension for VSCode (`glitch.glitch`)
4. Get the ESLint extension for VSCode (`dbaeumer.vscode-eslint`)
5. Launch the project via VSCode
	1. `Command + Shift + P` on Mac, `Ctrl + Shift + P` on Windows
	2. Select `Glitch: Show commands`
	3. Navigate to the project
	4. die